---
title: Rencontres, projections, lectures... Et réunion
date: 2023-05-08
author: David Soulayrol
---

## L'enquête GAFAM Nation

Issu de [Basta!](https://basta.media/), l’Observatoire des multinationales est *un média en ligne sur les grandes entreprises et plus généralement sur les pouvoirs économiques, ainsi que sur leurs relations avec le pouvoir politique*. En fin d'année dernière, il mettait en ligne son rapport  [GAFAM Nation](https://multinationales.org/fr/enquetes/gafam-nation/) mettant en lumière à quel point les dépenses de lobbying des géants du Web se sont développées ces dernières années. Une lecture fortement recommandée, qui démontre comment ces entreprises se positionnent aussi comme des partenaires incoutournables des institutions qui manquent fortement d'expertise et de moyens.

Il est aussi possible de découvrir ce rapport en écoutant l'interview de ses auteurs dans l'émission [Libre à vous #171](https://www.libreavous.org/171-le-rapport-gafam-nation)

## EntreeLibre#3

Au Centre des Abeilles, à Quimper, se tiendra du 18 au 20 mai la troisième édition d'EntréeLibre, c'est à dire un évènement gratuit et tourné vers le public le plus large et le plus néophyte quant aux usages et aux technologies du numérique. [Au programme](https://entreelibre.quimpernet.xyz/Programme), des conférences et des ateliers afin de *découvrir, comprendre et essayer*, et des rencontres avec les personnes qui connaissent (Stéphane Bortzmeyer) ou qui promeuvent la liberté et les outils éthiques avec talent (David Revoy, Gee, ...).

La venue de ces intervenants n'est pas sans nécessiter une certaine logistique, et bien que gratuit l'organisation a quelques frais à couvrir. Aussi, si vous avez quelque menue monnaie dans la poche, n'hésitez pas à participer à [leur financement](https://www.helloasso.com/associations/centre-des-abeilles/collectes/entreelibre-3).

## Au sujet de la cybersurveillance

Mardi 23 mai, à 20 h, [le groupe 113 de Lannion](https://www.net1901.org/association/AMNESTY-INTERNATIONAL-GROUPE-N%C2%B0113-DE-LANNION,2410662.html) organise [une projection-débat](https://www.ouest-france.fr/bretagne/lannion-22300/le-film-damnesty-sur-la-cybersurveillance-a-voir-le-23-mai-a-lannion-6a12166c-edce-11ed-b056-f847ef5daae0) dans la salle de conférences de l’espace Sainte-Anne. Le titre du documentaire proposé par Amnesty international ne peut qu'intéresser les visiteurs de ce site : *Cybersurveillance, un impact planétaire*. Le débat tournera autour de la surveillance numérique et des impacts sur les droits humains.

## Réunions de l'association

Le compte-rendu de notre dernière réunion, le jeudi 4 mai dernier, est [disponible ici](/cr/2023/cr-20230504.pdf). Pour l'essentiel, les discussions ont tourné autour de l'installation des premières machines. On approche du début de la migration ; il nous manque seulement les disques durs !

Notre prochaine réunion aura donc lieu le prochain premier jeudi, c'est à dire le **1er juin** à la Hutte, comme il se doit. L'ordre du jour figure sur [ce pad](https://apps.ti-nuage.fr/pad/p/2023-06-reunion-mensuelle). Vous pouvez à tout moment y indiquer votre présence et proposer les sujets que vous voulez voir aborder.
