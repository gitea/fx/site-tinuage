---
title: Le billet de mars
date: 2023-03-15
author: David Soulayrol
---

## Faites du Numérique à Plougrescant

Ça y est, la manifestation *Faites du Numérique* organisée à Plougrescant ce 11 mars a bien eu lieu. L'accueil était parfait et l'après-midi a été l'occasion de rencontrer quelques personnées curieuses. Elle a aussi surtout permis de faire mieux connaissance avec Florian Vasseur du FabLab de Lannion (que nous remercions pour le prêt des grands panneaux de l'Expo Libre), de Jean-Éric Monfort qui gère les cours proposés par l'association PATG sur Lannion ; de revoir Solène Trecherel, notre principale interlocutrice avec le département ; de rencontrer des élus de la municipalité et du canton de Plougrescant.

## Le guide d'auto défense numérique

En janvier sortait la sixième édition du [Guide d'auto-défense numérique](https://guide.boum.org/). Ce guide a pour objet de nous ouvrir les yeux sur le monde numérique, ses dangers, la surveillance toujours grandissante dont il est l'objet par des entités privées ou étatiques. Il apporte des éléments pour comprendre l'outil informatique, les réseaux. Et, de la manière de choisir un mot de passe correct jusqu'à l'utilisation de solutions telles que [Tor](https://www.torproject.org/), il fournit des conseils et des outils pour apprendre et mettre en œuvre des pratiques de protection appropriées à chaque situation.

Il est possible de le lire en ligne, ou de l'acquérir [en version papier](https://tahin-party.org/autodefense-numerique.html).

## Réunions de l'association

Le compte-rendu de notre dernière réunion, le jeudi 2 mars dernier, est [disponible ici](/cr/2023/cr-20230302.pdf). Le premier sujet de la soirée a été la préparation du support qui a servi à la présentation des logiciels libres à Plougrescant. Ce support est maintenant disponible [sur notre forge](https://apps.ti-nuage.fr/gitea/ti-nuage/talks).

Le second sujet de ce jeudi a été l'étude des achats de matériel à réaliser. Depuis, nous avons passé une première commande (grâce à l'avance des fonds de l'association **Koolisterik** que nous remercions chaleureusement), et nous installons une baie, un onduleur et un switch dans les jours à venir. Nous étudions maintenant comment migrer les premiers services.
 
Notre prochaine réunion aura lieu le **6 avril** à la Hutte, comme il se doit. Vous pouvez d'ores et déjà indiquer votre présence, proposer des sujets à aborder ou poser vos questions sur [ce pad](https://apps.ti-nuage.fr/pad/p/2023-04-reunion-mensuelle).
