---
title: Le billet de janvier
date: 2023-01-12
author: David Soulayrol
---

Nous notons qu'une conférence au Carré Magique ce mois-ci devrait intéresser les personnes sensibles aux coûts énergétique et écologique du numérique. Pour le reste, manifestations publiques et considération matérielles nous occupent beaucoup en ce début d'année.

## Conférence au Carré Magique

Dans le cadre des conférences organisées par le groupe *Rencontres*, M. Marc Collin (informaticien, enseignant à Rennes, membre du collectif *GreenIT*) intervient ce mercredi **25 janvier** au Carré Magique, à Lannion, pour discuter de sobriété numérique. 

> L'univers numérique dans lequel nous vivons a un impact environnemental non négligeable : le numérique émet désormais plus de CO² que le transport aérien dans le monde, et pour chaque Français, ce sont 6% de la consommation d'énergie primaire, et 4% des gaz à effet de serre émis. Et ces chiffres augmentent très rapidement avec les nouvelles applications, les objets connectés, etc... Il est donc important d'analyser à quel niveau se situent ces impacts : utilisateurs / réseaux / centres de données, de façon à pouvoir agir plus efficacement pour aller vers des systèmes plus vertueux et plus sobres !
> 
> Des groupes de recherche travaillent sur ce sujet, mais c'est aussi notre affaire, à chacun d'entre nous , consommateurs, voire surconsommateurs de technologies numériques…

Les renseignements pratiques sont disponibles sur le site du [Carré
Magique](https://www.carre-magique.com/spectacles/vers-la-sobriete-numerique).

## Réunions de l'association

Le compte-rendu de notre dernière réunion, ce jeudi 12 janvier, est [disponible ici](/cr/2023/cr-20230112.pdf). Il y est en particulier question de l'installation de notre matériel à Vieux-Marché et des nombreuses rencontres publiques de **Ti Nuage** dans les mois à venir.

Notez en particulier la manifestation *Faites du Numérique* **le 11 mars prochain à Plougrescant**. Cet événement voit le jour grâce à la distinction du projet dans le cadre l'appel *Construire le numérique en Côte d'Armor*, lequel a aussi récompensé notre association. Elle lieu dans la salle polyvalente Michel le Saint et son objet est de permettre aux visiteurs de découvrir différents usages et implications du numérique, en dehors des écrans.

Notre prochaine réunion aura lieu le **2 février** à la Hutte, comme il se doit. Vous pouvez d'ores et déjà indiquer votre présence, proposer des sujets à aborder ou poser vos questions sur [ce pad](https://apps.ti-nuage.fr/pad/p/2023-02-reunion-mensuelle).
