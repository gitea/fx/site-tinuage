---
title: Avril, tranquille
date: 2023-04-22
author: David Soulayrol
---

Si le mois de mai promet d'être doux aux salariés qui vont faire des semaines de trois ou quatre jours, c'est en avril que les administrateurs de **Ti Nuage** ont pris quelques vacances. Et ils ne sont pas les seuls au vu de la fréquentation de la dernière réunion : personne (sauf l'auteur de ces lignes, qui était bien présent et en a profité pour travailler pour l'association durant une bonne heure de calme).

Pont de compte-rendu ce mois-ci donc. Cependant le matériel arrive petit à petit et il sera présenté ici au fur et à mesure de sa mise en service. Les premiers outils numériques de l'association seront migrés des serveurs OVH sur nos propres machines dans les semaines à venir.

[**La Hutte**](https://lahutte.ti-nuage.fr), qui nous héberge, continue elle aussi de s'équiper. Mobilier et imprimante professionnelle sont venus compléter les locaux du lieu de travail partagé que propose l'association  **Les Castors Perchés** à Vieux-Marché.


## Réunions de l'association

Cette respiration dans notre emploi du temps ne change rien à nos habitudes. Notre prochaine réunion aura donc lieu le prochain premier jeudi, c'est à dire le **4 mai** à la Hutte, comme il se doit. L'ordre du jour figure sur [ce pad](https://apps.ti-nuage.fr/pad/p/2023-05-reunion-mensuelle). Vous pouvez à tout moment y indiquer votre présence et proposer les sujets que vous voulez voir aborder.
