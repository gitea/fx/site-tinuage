---
title: Assemblée d'automne
date: 2023-10-11
author: David Soulayrol
---

## Retour à Quimper

[Nous faisions part il y a quelques mois](/blog/2023/05/rencontres-projections-lectures...-et-reunion) de l'événement [Entrée Libre #3](https://www.centredesabeilles.fr/entree-libre3) qui s'est tenu au printemps à Quimper, au Centre des Abeilles. Cet événement, étudié pour s'adresser aux curieux et aux novices et pas seulement aux amateurs ou aux profesionnels des questions numériques, a été l'occasion de conférences instructives et d'ateliers qui le furent tout autant.

Par hasard, j'ai découvert que la conférence de Maïwann, *Justice sociale et environnementale*, était visible [sur l'instance PeerTube de ecologie.bzh](https://peertube.ecologie.bzh/w/bDZtndtRVeN3Aru44Xw4xP). Une belle occasion de prendre connaissance de faits et de chiffres qui mesurent l'impact des activités numériques sur la planète comme sur les relations entre citoyens. Et un bon complément à [la fresque du numérique](https://www.fresquedunumerique.org/).

## Assemblée Générale Ordinaire 2023

L'Assemblée Générale de l'association se tiendra le **jeudi 16 novembre**, à partir de 20 heures, à la Hutte, à Vieux Marché. Cette réunion, ouverte à tous, adhérents ou non, sera l'occasion de présenter le bilan de l'année passée, les perspectives à venir, et de reconduire le bureau de l'association.

Venez donc nombreux, avec vos questions, vos remarques, vos envies. Apportez de qui boire, de quoi grignotter, la soirée n'en sera que plus conviviale !

Notre rencontre sera aussi comme chaque fois l'occasion de discuter de services numériques et de logiciels libres. Par conséquent, exceptionnellement, **il n'y aura pas de premier jeudi en novembre**.

## Comptes-rendus

Le compte-rendu de notre dernier premier jeudi est [disponible ici](/cr/2023/cr-20231005.pdf).

Pour mémoire, il est possible de rencontrer des représentants de l'association tous les premier jeudi et, sauf si vraiment personne ne se présente et qu'il ne se passe rien, un compte-rendu est rédigé ensuite. Voici donc les compte-rendus rédigés depuis notre dernière Assemblée Générale au mois d'octobre 2022.

### 2022

- [3 novembre](/cr/2022/cr-20221103.pdf)
- [1er décembre](/cr/2022/cr-20221201.pdf)

### 2023

- [12 janvier](/cr/2023/cr-20230112.pdf)
- [2 février](/cr/2023/cr-20230202.pdf)
- [2 mars](/cr/2023/cr-20230302.pdf)
- [4 mai](/cr/2023/cr-20230504.pdf)
- [1er juin](/cr/2023/cr-20230601.pdf)
- [6 juillet](/cr/2023/cr-20230706.pdf)
- [7 septembre](/cr/2023/cr-20230907.pdf)
- [5 octobre](/cr/2023/cr-20231005.pdf)
