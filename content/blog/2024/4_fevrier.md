---
title: L'auto-hébergement
date: 2024-02-04
author: David Soulayrol
---

Hormis la conférence donnée ce mois de janvier au [café Théodore](http://www.cafetheodore.fr), ainsi que quelques évolutions dans le support des services, il n'y a pas grand chose à dire ce mois-ci de plus que ce qui se trouve comme de coutume dans [le compte-rendu de notre réunion du 1er février](/cr/2024/cr-20240201.pdf).

Ce calme est donc l'occasion pour moi d'inviter les personnes intéressées par nos activités à réviser ses bases en écoutant ou ré-écoutant [le numéro 109](https://www.libreavous.org/109) de l'émission [Libre à vous](https://www.libreavous.org/), dans laquelle il est question d'auto-hébergement. L'émission, hebdomadaire, est une action de l'association pour la défense et la promotion du Logiciel Libre (l'April), dont la lecture [du site](https://april.org/) est également pleine d'enseignements. Elle est diffusée sur [Radio Cause Commune](https://cause-commune.fm/), une radio associative dont les autres émissions pourront peut-être vous intéresser aussi.

Notre prochaine rencontre mensuelle aura lieu le **7 mars**, même lieu, même heure (c'est à dire bien sûr la Hutte, à Vieux-Marché, à 20h pour les plus ponctuels). L'ordre du jour se trouve sur [ce pad](https://apps.ti-nuage.fr/pad/p/2024-03-reunion-mensuelle), qui est aussi à votre disposition pour proposer les sujets que vous voulez voir aborder.
