---
title: Compte-rendu de l'Assemblée Générale du 6 octobre
date: 2022-10-10
author: David Soulayrol
---
Le compte-rendu de l'Assemblée Générale qui s'est déroulée le 6 octobre à la Hutte à Vieux-Marché est [disponible ici](/cr/2022/cr-ag-20221006.pdf). La réunion a vu la présence de sept adhérents, quatre autres s'étant excusés. Les activités menées cette année ont été passées en revue, puis la discussion s'est portée sur l'année à venir.

Le faible nombre d'administrateurs reste le point le plus inquiétant. L'association se porte sinon très bien, avec de multiples contacts et une progression d'un ou deux adhérents tous les mois. Le bureau a été renouvelé et se trouve aujourd'hui consolidé avec deux nouveaux volontaires.

La fin d'année sera occupée par la conclusion des deux principales actions réalisées cette année ; notre adhésion aux [CHATONS](https://chatons.org) et les résultats de notre réponse à l'appel à projets *Construire le numérique dans les Côtes d'Armor*. Par la suite, nous envisagerons les premières étapes pour installer nos propres serveurs dans la région.
