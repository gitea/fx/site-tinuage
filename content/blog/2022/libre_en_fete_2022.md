---
title: Libre en fête 2022
date: 2022-03-20
author: David Soulayrol
---

À l'occasion de l'initiative Libre en Fête, l'association Ti Nuage proposera un atelier dimanche 20 mars dans la salle Victor Hugo de la Mairie de Vieux-Marché, de 14 heures à 18 heures.

Cette rencontre est à destination de tous.

Fraîchement créée, l'association Ti Nuage souhaite organiser des rendez-vous récurrents pour aborder les questions relatives à la dépendance aux logiciels, la vie privée sur Internet, la frugalité numérique et les logiciels libres.

Cette première manifestation sera un moment informel et convivial pour rencontrer les personnes qui sont curieuses, souhaitent découvrir des alternatives libres aux logiciels et aux services courants, ou ont envie de s'investir dans l'aide et la maintenance de services pour les usagers du Trégor. 

    
https://www.agendadulibre.org/events/25008
