---
title: Compte-rendu de la réunion du 28 juin
date: 2022-07-02
author: David Soulayrol
---
Les adhérents étaient invités à une nouvelle réunion le mardi 28 juin. Outre le petit tour habituel sur l'évolution des services et des démarches de l'association pour intégrer le collectif des [CHATONS](https://www.chatons.org), il y a été question de [l'appel à projets du département](https://cotesdarmor.fr/numerique) concernant le Numérique. Le compte-rendu de cette soirée est [disponible ici](/cr/2022/cr-20220628.pdf).

Les réunions mensuelles devraient se formaliser un peu mieux bientôt avec une date récurrente et stable. Les adhérents sont appelés à se prononcer très bientôt sur leurs préférences quant au jour et à l'heure.

Le logiciel de messagerie **Mattermost** est abandonné en faveur du réseau fédéré **XMPP**. Tous les adhérents peuvent maintenant se connecter à l'aide d'une grande variété de logiciels avec leurs identifiants habituels. Un réseau fédéré, qu'est-ce que c'est ? Voilà un beau sujet à découvrir lors d'un atelier futur pour les personnes intéressées.
