---
title: Compte-rendu de la réunion du 1er septembre
date: 2022-09-05
author: David Soulayrol
---
Le compte-rendu de notre deuxième « premier jeudi du mois » est [disponible ici](/cr/2022/cr-20220901.pdf). Une nouvelle fois, la rencontre s'est faite en petit comité et l'ambiance était à la détente, les dossiers importants à gérer étant pour l'essentiel derrière nous. Un document court donc, pour un petit tour d'horizon des dernières activités.

Ce compte-rendu est également l'occasion de revenir sur le forum aux associations de Plouaret, où nous avons enregistré une nouvelle adhésion et fait des rencontres intéressantes.
