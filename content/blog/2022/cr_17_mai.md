---
title: Compte-rendu de la réunion du 17 mai
date: 2022-05-23
author: David Soulayrol
---
Les adhérents étaient invités à une réunion le mardi 17 mai pour faire le tour de l'état des services et leurs évolutions envisagées, connaître les démarches de l'association pour intégrer le collectif des [CHATONS](https://www.chatons.org) ou tout simplement faire mieux connaissance. Le compte-rendu de cette soirée est [disponible ici](/cr/2022/cr-20220517.pdf).

La possibilité de faire des réunions récurrentes afin d'organiser des ateliers ou des débats sur des sujets liés aux logiciels libres ou aux technologies numériques a de nouveau été discutée. Si rien n'est ressorti de clairement encore, une prochaine réunion est déjà planifiée pour la fin du mois de juin. Elle sera annoncée ici lorsque la date aura été fixée par les adhérents.
