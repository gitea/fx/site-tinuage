---
permalink: false
title: Conditions Générales d’Utilisation des services Ti Nuage - CGU
---
Ti Nuage propose des services en ligne libres et éthiques afin de fournir une alternative aux produits des GAFAM ou « Géants du web », respectueuse des données et de l'intimité de chacun. Ces services sont assurés par une association loi 1901 et reposent essentiellement sur le bénévolat d'une poignée d'administrateurs, sur un modèle de *best effort* en cas d’incident par exemple, sans garantie de continuité de services ou autre.

Vous pouvez lire nos mentions légales sur la [page dédiée](/mentions_legales.html).

En utilisant un service Ti Nuage, vous acceptez d’être lié par les conditions d'utilisation suivantes. Ces conditions sont celles en vigueur depuis le 06/11/2023. Elles sont disponibles à l'adresse https://www.ti-nuage.fr/cgu.html. L'historique des modifications est visible [sur notre forge](https://apps.ti-nuage.fr/gitea/ti-nuage/site/commits/branch/master/content/cgu.md).

## Services

Ti Nuage permet à tout internaute un accès gratuit à une palette des services numériques et notamment à :

- des outils de rédaction partagée ;
- un gestionnaire de sondage ;
- des moyens pour partager des fichiers.

Si vous adhérez à l'association, vous pouvez bénéficier en outre des services suivants :

- une boîte de courriel ;
- des listes de diffusions ;
- l'hébergement d'un site Web ;
- un outil d'hébergement de fichier et de collaboration (Nextcloud) ;
- une forge logicielle (Gitea) ;
- une adresse sur le réseau de discussion instantanée fédéré XMPP ;
- un gestionnaire sécurisé de mots de passe (Bitwarden).

L'association Ti Nuage se réserve le droit à tout moment de modifier ou d’interrompre, temporairement ou définitivement, l'un des services précédents avec ou sans préavis. Ti Nuage ne pourra être responsable envers vous ou tout tiers pour ces modifications, suspensions ou interruptions de service.

Les adhérents de l'association peuvent être avertis de tous changements liés aux services disponibles ou aux Conditions d'Utilisation en s'inscrivant à la liste d’informations services@listes.ti-nuage.fr.

## Périmètre géographique

Parce qu'elle organise des rencontres régulières avec ses adhérents, Ti Nuage s'adresse avant tout aux personnes physiques ou morales résidant dans la région du Trégor en Côtes d'Armor ou ayant un lien avec la région. Toutefois, il est possible d'utiliser les services et d'adhérer en dehors de ce périmètre.

La langue d'échange pour le support, les ateliers et les réunions est le français.

## Responsabilité de Ti Nuage

L'association Ti Nuage a qualité d’hébergeur par application des dispositions de l’article 6 de la Loi n° 2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique. À ce titre, Ti Nuage ne peut être tenue responsable que pour les contenus qu'elle a elle même mis en ligne. Elle ne peut être tenue responsable pour les contenus ou les liens publiés par les utilisateurs et utilisatrices de ses services, sauf en cas de non retrait sur signalement de contenus illicites, tel que le prévoit la loi.

Ti Nuage se réserve le droit de poursuivre toute personne physique ou morale pour tout fait de nature à lui porter préjudice.

L’utilisation des services se fait à vos propres risques. Les services sont fournis tels quels.
Ti Nuage ne garantit pas que :

- les services répondront à vos besoins spécifiques ;
- les services seront ininterrompus ou exempt de bugs ;
- les erreurs dans les services seront corrigées.

Vous comprenez que la mise en ligne des services ainsi que de votre contenu implique une transmission (en clair ou chiffrée, suivant les services) sur divers réseaux. Vous comprenez que la publication de contenu sur certains services implique qu'il soit accessible à toutes et tous.

Vous comprenez et acceptez que Ti Nuage ne puisse être tenue responsable de tous dommages directs, indirects ou fortuits, comprenant les dommages pour perte de profits, de clientèle, d’accès, de données ou d’autres pertes intangibles (même si Ti Nuage est informée de la possibilité de tels dommages) et qui résulteraient de : 

- l’utilisation ou de l’impossibilité d’utiliser le service ;
- l’accès non autorisé ou altéré de la transmission des données ;
- les déclarations ou les agissements d’un tiers sur le service ;
- la résiliation de votre compte ;
- toute autre question relative au service.

L’échec de Ti Nuage à exercer ou à appliquer tout droit ou disposition de ces Conditions Générales d’Utilisation ne constitue pas une renonciation à ce droit ou à cette disposition. Les Conditions d’Utilisation constituent l’intégralité de l’accord entre vous et Ti Nuage et régissent votre utilisation du service, remplaçant tous les accords antérieurs entre vous et Ti Nuage (y compris les versions précédentes des Conditions Générales d’Utilisation).

## Votre responsabilité

La création d'un compte, lorsqu'elle est possible, n'est autorisée que pour des personnes physiques ou morales. Vous ne pouvez vendre, échanger, revendre, ou exploiter dans un but commercial non autorisé aucun de ces comptes. Les comptes créés par des robots ou tout autre méthode automatisée pourront être supprimés sans mise en demeure préalable.

Vous êtes responsable de la sécurité de votre compte et de votre mot de passe. Ti Nuage ne peut pas et ne sera pas responsable de toutes pertes ou dommages résultant de votre non-respect de cette obligation de sécurité.

Vous êtes responsable de tout contenu affiché et de l’activité qui se produit sous votre compte.

Vous ne devez pas transgresser les lois de votre pays. En particulier, vous ne pouvez pas utiliser le service à des fins illégales ou non autorisées. Vous ne devez pas transmettre des vers, des virus ou tout autre code de nature malveillante.

Le non-respect de l’une de vos obligations pourra entraîner la résiliation de vos comptes. Votre responsabilité pourra être engagée en cas de manquement à l’une de vos obligations ayant causé un préjudice aux services Ti Nuage.

## Données à caractère personnel

Lorsque vous adhérez à l'association, vous vous engagez à fournir des informations sincères et exactes concernant votre état civil et vos coordonnées. Ces informations sont nécessaires à Ti Nuage pour pouvoir vous contacter, et pour cela seulement.

Certains services enregistrent automatiquement dans des journaux des informations concernant l'utilisation que vous en faites ou des données permettant de vous identifier (par exemple : type de navigateur, adresse IP, date et heure de l’accès, URL de référence). La loi impose la conservation de ces journaux informatiques pour une durée de un an.

L'utilisation de certains services nécessite la création d'un compte et de certaines informations personnelles. Dans ce cas, un nom et un prénom peuvent être requis pour le bon fonctionnement du logiciel, mais il n’est pas nécessaire qu’ils révèlent votre véritable identité.

Toutes les données recueillies ne sont ni vendues, ni transmises à des tiers, et ne le seront jamais.

Conformément aux dispositions de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, dans sa version issue de la loi n°2018-493 du 20 juin 2018 et de son décret d’application, l'association Ti Nuage vous garantit un droit d’opposition, d’accès et de rectification sur les données nominatives vous concernant. Pour exercer tout droit relatif à vos données à caractère personnel, vous pouvez contacter support@ti-nuage.fr.

## Localisation des données

Vos données sont hébergées sur nos propres machines, à Vieux-Marché.

Cependant, des sauvegardes quotidiennes et chiffrées sont stockées chez un hébergeur tiers. (Des tests de restauration de ces sauvegardes sont effectués régulièrement). En outre, il est possible que pour assurer une continuité de service en cas de panne vos données soient aussi copiées chez un autre hébergeur. Les prestataires auxquels nous faisons appel dans ces deux cas sont les suivants.

- **Sas OVH**. `2 rue Kellermann, 59100 Roubaix - France`
- **Hetzner Online GmbH**. Industriestr. 25, 91710 Gunzenhausen - Germany

## Cookies (témoins de connexion)

Les services Ti Nuage déposent sur votre ordinateur des cookies strictement nécessaires au fonctionnement du service. Aucun pistage n’est réalisé au moyen de ces cookies.

## Droit d’auteur sur les contenus

Les textes, images, sons, vidéos, ou tout autre élément que vous téléchargez ou transmettez depuis votre compte vous appartiennent pleinement et sont de votre responsabilité. Vous ne pouvez pas, en particulier,  envoyer, télécharger, publier, distribuer, diffuser tout contenu illégal, diffamatoire, harcelant, abusif, frauduleux, contrefait, obscène ou autrement répréhensible.

Ti Nuage ne revendique aucun droit sur vos données, ni ne les utilise autrement que pour vous fournir ses services de stockage et de partage. En revanche, et conformément aux obligations en qualité d’hébergeur de l'association, Ti Nuage pourra procéder au retrait de tout contenu dès le moment où elle aura eu connaissance de son caractère manifestement illicite. Ti Nuage se réserve également le droit d'empêcher la diffusion ou de supprimer tout contenu qui porterait préjudice au bon fonctionnement des services. Pour signaler un tel contenu ou un abus vous pouvez contacter support@ti-nuage.fr.

Sauf mention contraire, tous les contenus des sites de Ti Nuage, hors les données des adhérents ou des utilisateurs anonymes, appartiennent à l'association.

## Migration, résiliation et suppression

Vous pouvez à tout moment exporter vos données via les fonctions disponibles sur le logiciel fournissant chaque service. En cas de problème, vous pouvez contacter support@ti-nuage.fr pour obtenir de l'aide.

Vous pouvez à tout moment demander la suppression de vos comptes et des données associées. Ti Nuage s’engage à répondre à la demande dans un délai raisonnable en tenant compte des limites possibles des services utilisés.

Ti Nuage a le droit de suspendre ou de résilier les comptes que vous avez ouvert sur un service et de refuser toute utilisation actuelle ou future à ce service. Cette résiliation entraînera la désactivation de l’accès à votre compte, et dans la mesure du possible la restitution de l'ensemble de vos contenus. Ti Nuage n’a aucune obligation de résultat quant à la restitution de vos données.

Ti Nuage se réserve également le droit de résilier votre compte sur un service gratuit s’il est présumé abandonné, ou sur vos services adhérents si vous vous êtes retiré ou avez été exclu de l'association. En dehors du cas de l'exclusion, cette résiliation sera effective seulement après l’expiration d’un délai de 8 jours suivant une demande par courriel de justifier de l’utilisation du compte.

## Obligation de communication de données

Ti Nuage ne communique des données concernant les utilisateurs et utilisatrices de ses services que dans les cas où cette communication est rendue obligatoire par les dispositions législatives et réglementaires.

## Droit applicable et juridiction compétente

Les services et leurs Conditions Générales d’Utilisation sont régis par la loi française, quel que soit le lieu d’utilisation. En cas d’échec des solutions amiables ou dans l’absence de leur recherche, les tribunaux français sont seuls compétents pour répondre de tout différend né des présentes dispositions.
