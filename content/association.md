---
permalink: false
title: L'association
---
L'association **Ti Nuage** est née de la volonté de proposer dans le Trégor des services en ligne indépendants et respectueux de la vie privée, et d'assister les personnes qui souhaitent mieux comprendre les technologies numériques, leurs usages, leurs risques, et leur poids sur nos vies. Son objet est :

- de mettre à disposition des services en ligne basés sur des logiciels libres, garantissant la confidentialité des données des utilisateurs, et d'héberger ces données dans le Trégor ;
- de sensibiliser les utilisateurs à la protection de leur vie privée sur Internet ;
- de promouvoir l'utilisation de logiciels libres ;
- d'organiser des événements sur ces thèmes et de former à l'utilisation des technologies de l'information et de la communication ;
- d'utiliser et promouvoir une informatique peu consommatrice en énergie.

Pour remplir ces objectifs, l'association fournit des services en ligne libres et respectueux de l'utilisateur. Elle organise aussi des rencontres régulières pour discuter des thèmes qu'elle promeut, découvrir les besoins des communautés du Trégor, répondre aux questions des usagers.

Adhérer à l'association permet de profiter de services particuliers (adresse de courriel, hébergement de site Web, ou compte personnel sur la plateforme de production et de collaboration NextCloud) et de participer à cette dynamique. Chaque adhérent possède une voix et peut apporter son aide et ses idées sur toutes les activités de l'association.

Les statuts se trouvent à [cette adresse](/statuts.pdf).

Les modalités d'inscription se trouvent [sur le Wiki](https://wiki.ti-nuage.fr/fr/Association/Adhesion).
