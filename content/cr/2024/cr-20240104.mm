.ND 04/01/2024
.TL
Compte-rendu de Réunion
.br
Jeudi 4 janvier 2024 à Vieux-Marché
.AU "David"
.MT 4
.hl
.PRESENT "David, Nourdine"
.HU "C'est Noël !"
Cette année, notre Président a endossé le rôle du gros bonhomme rouge pour distribuer au sein de l'association quelques cadeaux surprise.
.P
Pour commencer, l'association a acquis un exemplaire du livre
.I "Ada et Zangemann"
dont nous avons causé le mois dernier sur le blog.
L'ouvrage\*F
.FS
.URL https://cfeditions.com/ada
.FE
, libre à plus d'un titre, introduit les notions de logiciel libre, de développement et d'appropriation des outils numériques aux plus jeunes.
.P
Dans un même esprit éducatif, mais pour les plus grand, un jeu de métacartes\*F
.FS
.URL https://www.metacartes.cc
.FE
est maintenant disponible à la Hutte.
Ces cartes présentent différents aspects et objectifs liés au numérique et servent de support de discussion pour apprendre ou progresser dans une démarche de libération et de reconquête des outils.
.P
Enfin, Nourdine a récupéré des ordinateurs portables en état de marche.
S'il ne s'agit pas exactement de modèles tout récents, ce sont cinq machines qui sont disponibles pour ceux qui souhaitent disposer de matériel pour des travaux de bureautique usuels ou l'utilisation d'Internet.
.HU "Proposition de VM complètes aux adhérents"
Question cadeaux, notre serveur a également eu sa part avec la multiplication par 6 (!) de sa capacité en mémoire vive, soit 96 giga-octets.
Nourdine envisage donc de profiter des capacités disponibles en proposant aux adhérents l'installation d'une machine virtuelle pour un usage complètement personnalisé.
Il s'agit pour les utilisateurs expérimentés, ou simplement curieux, de disposer d'un serveur apte à accueillir n'importe quel usage.
Attention cependant, si nous pouvons apporter des conseils sur la manière d'administrer un tel serveur, il n'est pas question de fournir un support technique complet.
.HU "Changement de fournisseur Internet"
Ce sera bientôt le premier anniversaire de l'installation de la fibre à Lannion, et donc la fin du tarif avantageux dont nous profitons.
Pour mémoire, nous avons opté pour Free exactement pour cela, une fois nous être assurés que leur prestation technique convenait à nos besoins bien sûr.
Pourtant, notre fournisseur d'accès à Internet (FAI) de choix serait la FDN\*F
.FS
.URL https://www.fdn.fr
.FE
, une structure associative établie de longue date et qui fournit un service de qualité.
Le prix à terme de l'abonnement chez Free devant être proche de celui de la FDN, nous devrions bientôt changer pour ce dernier.
.HU "Support de conférence au sujet des CGU"
David a terminé la rédaction d'un support de présentation pour discuter des Conditions Générales d'Utilisation (CGU) et des règles de confidentialité couramment rencontrées sur Internet.
Cette présentation aborde également la question de la confiance, régulièrement levée par des personnes rencontrées sur des salons ou dans des GULL.\*F
.FS
.I "Groupes d'Utilisateurs de Logiciel Libre"
.FE
Nous avons relu ensemble le texte de cette présentation, et il devrait être possible d'organiser une conférence avec ce support prochainement.
.HU "Changement de domiciliation bancaire"
Nous en avons discuté plusieurs fois ; le Crédit Coopératif pratique des prix importants pour une structure comme la nôtre, et ils ne sont pas particulièrement aidant non plus dans nos rares démarches.
Nous rencontrerons donc le Crédit Mutuel de Bretagne à Plouaret courant Janvier pour probablement changer de domiciliation bancaire rapidement, et dans le même temps ouvrir un livret pour épargner les sommes que nous avons d'avance.
