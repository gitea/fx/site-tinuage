.ND 06/07/2023
.TL
Compte-rendu de Réunion
.br
Jeudi 6 juillet 2023 à Vieux-Marché
.AU "David"
.AST "Résumé"
.AS
Une réunion entre administrateurs, faute de participants. Nous épargnerons les détails techniques au lecteur qui sait déjà que nous travaillons sur les machines reçues à Vieux-Marché. Pour le reste, il a été question de possibles nouveaux adhérents, d'argent et de partage de connaissances.
.AE
.MT 4
.hl
.PRESENT "David, Nourdine"
.HU "Besoins exprimés par le réseau Cigales Bretagne"
Les plus assidus se souviendront peut-être que les Cigales de Bretagne\*F
.FS
.URL https://www.cigales-bretagne.org
.FE
se sont tournées vers nous pour trouver une solution à leur besoin de listes de diffusion. Ce besoin était complexe au premier abord, mais après plusieurs échanges sa définition est devenue plus claire et il est finalement possible pour Ti Nuage d'y répondre (et probablement de démarrer dès que possible).
.P
Il est donc question de gérer deux sous-domaines des Cigales, qui seraient exploités uniquement pour la diffusion de courriel à l'aide d'alias. Autrement dit, nous ferions pour eux du routage, pas de stockage.
.HU "Subventions et ressources"
Nous avons complété les achats pour l'installation de tous les serveurs envisagés, sauf la machine devant supporter NextCloud, les comptes utilisateurs, etc. Il reste actuellement environ 1300€ sur la subvention octroyée par le département. Et comme annoncé le mois dernier, nous devons également recevoir une nouvelle subvention de 1600€ (montant disponible après ponction de la commission pour Roxane qui fait pour nous les démarches de recherche de subvention).
.P
David soumet l'idée d'ouvrir un livret pour mettre de côté l'argent destiné à servir seulement en cas d'accident. Il convient de vérifier la faisabilité et les conditions pour cela.
.HU "Abonnement magazine spécialisé"
Nourdine propose l'abonnement à un ou plusieurs magazines. MISC en particulier propose des articles pointus sur la sécurité ou la configuration de systèmes et de réseaux. Une rapide exploration des sommaires récents montre que Linux Pratique propose aussi de nombreux sujets en rapport avec l'administration, alors que Linux Mag met davantage l'accent sur la programmation. Il est envisagé de prendre un abonnement complet à ces deux magazines pendant un an pour juger de la pertinence de tel ou tel titre à long terme.
.P
Ces magazines seront mis à disposition des adhérents et des personnes qui nous rendent visite à la Hutte. David propose d'apporter aussi au même endroit les magazines dont il dispose déjà (d'anciens numéros de Linux Mag, Linux Pratique et Login:.) pour que chacun puisse les consulter.
.HU "Camp CHATONS"
Le camp CHATONS aura lieu du jeudi 3 au lundi 7 août 2023 au Château du Vergnet. Le camp est l'occasion de faire avancer plus rapidement les chantiers au sein de l'association, tout en permettant de se découvrir les uns les autres dans une ambiance agréable. Si Nourdine ou David ont envisagé d'y participer un temps, la charge de travail de chacun et les collisions de dates font que cela ne se fera pas cette année. Toutefois cette rencontre sera étudiée de nouveau l'an prochain. Il est aussi envisageable que si l'association dispose de fonds suffisants elle participe au coût du trajet.
