.ND 16/11/2023
.TL
Compte-rendu de l'Assemblée Générale
.br
du jeudi 16 novembre 2023 à Vieux-Marché
.AU "David"
.MT 4
Les membres de l'association
.B "Ti Nuage"
ont été convoqués par courrier électronique le 11 octobre 2023 pour une Assemblée Générale ce jeudi 16 novembre à 20 heures à la Hutte, 24 place Anjela Duval à Vieux-Marché.
L'objet de cette réunion est de discuter du bilan de l'année écoulée et de dresser les objectifs pour la suivante.
.P
Le Président de séance est Nourdine, le secrétaire ; David.
.hl
.PRESENCE "Présent(e)s" "5 membres (David, Gérard, Mathias, Nourdine, Vincent L.)"
.HU "Bilan moral"
L'association, déclarée à la sous-préfecture de Lannion le 18 octobre 2021, a maintenant deux ans d'existence.
Elle accueille désormais environ 32 adhérents individuels et 11 structures (chacune de ces dernières ayant 2 comptes en moyenne), pour un total exact de 57 comptes.
Ces chiffres ne prennent pas en considération les adhésions arrivées récemment à expiration.
Le bureau s'interroge encore sur la manière de gérer les relances et les comptes abandonnés.
.TS
allbox centre tab(|);
nene.
2021|11
2022|25
2023|10
.TE
.TB "Nouveaux adhérents par année"
.P
L'année 2022 s'est terminée sur deux sujets importants :
.BL
.LI
le vote positif pour notre intégration dans les CHATONS\*F
.FS
.URL "https://www.chatons.org"
.FE
; si ce n'était pas une totale surprise, cette étape était importante pour nous ;
.LI
l'obtention du prix de l'innovation et de l'octroi d'une subvention de 5000€ pour notre réponse à l'appel à projets
.I "Construire le numérique dans les Côtes d'Armor"
\*F
.FS
.URL "https://cotesdarmor.fr/numerique"
.FE
lancé par le département.
.LE
.P
Ce dernier point, longuement préparé au cours de l'année précédente, a été une bonne surprise et nous a permis de concrétiser plus rapidement que prévu notre objectif d'installer les services et les données des adhérents à Vieux-Marché.
Concrètement, il nous a cependant fallu avancer des fonds, et l'association
.B "Koolisterik"
nous a aidé en nous faisant un prêt gracieux sur l'année que nous avons remboursé cet automne.
Et par ailleurs, Roxane a obtenu pour nous une subvention du FDVA\*F
.FS
.URL "https://www.associations.gouv.fr/FDVA.html"
.FE
de 2000€.
Comme convenu avec elle, 20% de cette somme (400€) lui reviennent pour le travail de recherche de subvention qu'elle réalise pour nous.
.P
La définition et l'achat du matériel nous a ensuite occupé une bonne partie de l'année.
À côté d'un serveur performant pour le support des principaux services de stockage et de logiciels pour les adhérents, nous avons opté pour des solutions légères à base de nano-ordinateurs.
Cette solution, économique, est aussi très intéressante en terme de consommation électrique dès lors qu'un logiciel ne nécessite pas particulièrement de puissance en terme de processeur.
La relocalisation des services s'est faite à partir de la fin de l'été après quelques tests et tatonnements.
À ce jour, toutes les données des utilisateurs ainsi que les services proposés sont hébergées à Vieux-Marché, hormis les sites Web et les listes de diffusion.
.P
L'année avait commencé également avec le choix du prestataire pour la fibre.
C'est Free qui a été retenu pour le moment, d'une part pour profiter d'un tarif avantageux cette première année, et d'autre part pour les possibilités techniques offertes.
Un accord a été passé avec l'association
.B "Les Castors Perchés"
:
.B "Ti Nuage"
gère et partage la connexion Internet et rembourse sa consommation électrique mais ne paie aucun loyer.
.P
Notre seule activité hors les murs cette année a été la manifestation
.I "Faites du numérique"
à Plougrescant le 4 mars.
Sa préparation a été longuement relayée dans les comptes-rendus.
La journée, très sympathique, a été l'occasion de discuter longuement avec le public, avec Florian du
.B "FabLab"
de Lannion, avec des élus déjà rencontrés et d'autres, avec
.B "PATG"
\*F
.FS
.URL "https://patginformatique.bzh/"
.FE
\&.
.P
Enfin, l'association s'est abonnée aux titres
.I "MISC" et
.I "Linux Pratique"
des éditions Diamond.
L'objectif est de proposer à la fois aux administrateurs quelques articles intéressants pour une veille technique, et aux adhérents ou visiteurs des lectures au sujet de logiciels libres.
Ces titres ont été choisis pour la pertinence de leurs articles a priori.
La formule pourra être ré-avaluée dans un an au vu du contenu des numéros parus.
\R'Hu +1'
.HU "Perspectives"
Il reste encore beaucoup de travail sur les services, en particulier la migration de l'hébergement des sites Web.
L'opération prend du temps parce que nous profitons de cette relocalisation pour revoir la structure du système et apporter une plus grande isolation des comptes.
Bref, nous nous faisons la main sur Docker.
.P
Par ailleurs, quelques idées lancées lors de la dernière Assemblée Générale, ou plus récemment, n'ont pas encore été développées, la faute en général à l'absence d'un volontaire pour s'en charger.
Elle méritent pourtant d'être prise en charge.
.BL
.LI
Gilles avait proposé un outil pour un hébergement Web simplifié pour les associations ;
.LI
David a proposé d'écrire un portail simple pouvant être utilisé comme page d'accueil et fournissant un champ de recherche, des liens vers une sélection d'outils libres ;
.LI
Nourdine avait soumis la possibilité d'ouvrir des comptes de courrier électroniques à certains non adhérents, mais l'idée n'a pas été développée.
.LE
.P
David avait proposé quelques sujets de conférences, et l'un d'entre eux a donné lieu à la création d'un support qui a été utilisé à Plougrescant.
Leur développement tarde faute de temps, mais progresse.
Ces sujets pourront être proposés au
.B "Fab Lab" ,
qui attend de notre part des propositions de conférence, ou aux associations
.B "PATG"
ou
.B "Infothema"
pour apporter un complément aux cours qu'elles dispensent.
Il pourrait être intéressant de répondre aussi au collège de Plouaret qui nous avait abordé l'an passé.
De manière générale, l'association a été peu présente sur les lieux publics ces derniers mois et il y a moyen de faire mieux.
.P
Nourdine étudie la possibilité d'installer des outils tels que Mobilizon pour répondre à des besoins exprimés par certaines institutions locales.
.P
Enfin Mathias a lancé il y a quelques mois l'idée de rencontres entre CHATONS proches. Éric, de
.B "Infothema" ,
qui nout fait l'amitié de participer à la réunion, approuve l'idée.
.HU "Vote"
Le bilan moral est approuvé à l'unanimité.
\R'Hu -1'
.HU "Rapport financier"
Comme prévu, les dépenses hors investissement de l'association ont été un peu plus importantes cette année du fait de l'augmentation des charges le temps que la migration se fasse.
La subvention du FDVA a été la bienvenue afin que le compte ne soit pas trop près du 0 au moment des derniers achats de matériel.
.P
Les tableaux suivants mettent en évidence la pérennité de notre modèle de financement une fois que les charges liées à nos prestataires auront été remplacées par celles induites par notre installation.
La consommation électrique, dans le second cas, est une projection majorée sur la base des premières mesures réalisées ces dernières semaines.
.TS
allbox centre tab(|);
lw(8cm)ne.
OVH (Noms de domaines et multiples VPS)|53,71
Hetzner (Backup 1To)|3,84
Frais bancaires|7,30
Assurance|8,33
Don pour TeDomum contre service Peer Tube|2
.TE
.TB "Charges mensuelles courantes avec OVH"
.TS
allbox centre tab(|);
lw(8cm)ne.
Free (Abonnement FTTH)|19,99
Charges (Estimation de la consommation électrique)|20
OVH (Noms de domaines et VPS Sympa)|7,03
Hetzner (Backup 1To)|3,84
Frais bancaires|7,30
Assurance|8,33
Don pour TeDomum contre service Peer Tube|2
.TE
.TB "Charges mensuelles induites par l'hébergement à Vieux-Marché"
\R'Hu +1'
.HU "Vote"
Le rapport financier est approuvé à l'unanimité.
.P
Il est également voté de reconduire le montant actuel des cotisations.
\R'Hu -1'
.HU "Renouvellement du Bureau"
L'Assemblée vote à l'unanimité la reconduction de Nourdine en tant que Président et David pour le poste de secrétaire.
Mathias, trésorier depuis la dernière Assemblée Générale mais qui n'a pu pleinement officier jusqu'à présent à cause des procédures bloquantes de la banque, est confirmé dans son poste.
Vincent L. consent à rester dans le Bureau pour continuer à apporter son avis.
Gilles est radié du fait de son silence à peu près complet depuis la dernière Assemblée.
.P
Le nouveau Bureau se compose donc maintenant comme suit :
.BL
.LI
Président : Nourdine Gernelle
.LI
Secrétaire : David Soulayrol
.LI
Trésorier : Mathias Mantello
.LI
Membres ordinaires : Vincent Lhoutellier
.LE
.SP 4
.P
L'Assemblée est levée avant 22h par épuisement de l'ordre du jour.
