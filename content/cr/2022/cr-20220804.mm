.ND 04/08/2022
.TL
Compte-rendu de Réunion
.br
Jeudi 04 août 2022 à Vieux-Marché
.AU "David, Mathias"
.AST "Résumé"
.AS
Ce jeudi était la première de nos réunions mensuelles à date fixe à la Hutte.
L'essentiel de la discussion se porte sur trois points\ : l'état de notre dossier pour l'appel à projets numériques du département, notre candidature au CHATONS\*F
.FS
Le Collectif des Hébergeurs Alternatifs, Transparents, Neutres et Solidaires\ :
.URL https://www.chatons.org
.FE
et l'étude des actions que nous pouvons mener durant les événements à venir, en particulier la Fête des Possibles.
(Mais si vous lisez jusqu'au bout vous apprendrez aussi la date de la prochaine Assemblée Générale de l'association.)
.AE
.MT 4
.hl
.PRESENT "David, Mathias, Nourdine"
.HU "Appel à projets pour le Numérique du CD22"
L'association progresse correctement pour répondre à l'appel à projets du département dans le cadre de sa démarche
.I "Construire le numérique en Côtes d’Armor" .
La description de cet appel et les raisons pour lesquelles nous y répondons se trouvent dans le précédent compte-rendu\*F
.FS
.URL "https://ti-nuage.fr/cr/2022/cr-20220628.pdf"
.FE
\&.
.P
La réponse doit comporter un dossier, une lettre de recommandation d'une autorité publique et une vidéo de deux minutes. Les éléments de la vidéo ont été tournés avec le concours des associations
.I "Liratouva"
et
.I "La convergence des Loutres" ,
ainsi que de Gérard\ : merci à eux.
Vincent doit procéder au montage dans les tous prochains jours.
Nourdine a pris contact avec la mairie et nous attendons une lettre de leur part.
Le contenu du dossier est pour l'essentiel prêt\ : David et Nourdine finissent d'assembler les argumentaires et le détail chiffré du projet.
.HU "CHATONS"
En quatre mots\ ; la candidature est posée. (Youpi.)
Le calendrier des actions à venir a été annoncé en juillet\ :
.BL
.LI
du 16 octobre au 15 novembre, les candidatures de cette quinzième portée sont analysées\ ;
.LI
du 16 au 30 novembre, les candidats sont invités mettre à jour leur candidature en tenant compte du résultat des audits\ ;
.LI
enfin, les votes ont lieu 1er au 20 décembre, et les résultats sont annoncés le 21 décembre.
.LE
.P
Par ailleurs, Nourdine est heureux d'annoncer que
.I MailJet
n'est plus utilisé.
Il a écrit un document afin d'expliquer comment il appartient à chacun de conserver la bonne réputation de notre adresse IP\*F
.FS
.URL "https://wiki.ti-nuage.fr/Services/Communication/Courriel#sender-score-et-délivrabilité-des-e-mails"
.FE
, laquelle nous permet de ne pas avoir à déployer de solution complexe pour router notre courrier.
.HU "Fête des Possibles"
Cet événement est un ensemble de manifestations organisées chaque année pour
.I "rendre visibles les initiatives citoyennes qui construisent une société plus durable, humaine et solidaire"\*F
.FS
.URL "https://fete-des-possibles.org/"
.FE
\&.
Il a lieu du 9 au 25 septembre.
.P
Différentes associations promouvant le logiciel libre participent à cette fête.
Cependant
.B "Ti Nuage"
manque de visibilité et de moyens aujourd'hui pour organiser elle-même une manifestation.
Nous proposons de nous renseigner simplement auprès des associations proches, telles
.I "La convergence de Loutres" ,
pour savoir si elles comptent participer et ce qu'elles pensent organiser.
Alors nous pourrons étudier s'il serait judicieux d'envisager quelque chose en commun.
.HU "Forum des associations"
Nous l'avions presque oublié, mais nous avons un forum à préparer à Vieux-Marché.
Nourdine doit se renseigner pour savoir exactement le lieu, les conditions et les plages horaires afin de préparer un calendrier pour que les volontaires puissent s'organiser pour tenir le stand.
.P
Mathias profite de la rédaction du compte-rendu pour suggérer également que l'on demande à la mairie de nous lister sur leur site\*F
.FS
.URL "https://www.commune-levieuxmarche.com/la-vie-associative"
.FE
\&.
.P
Il est par ailleurs une idée intéressante qui avait été laissée de côté : demander au FabLab s'il serait possible de partager un coin de table pour le forum de Lannion (le samedi 3 septembre) auquel nous ne pouvons participer, ou bien leur donner quelque chose pour présenter
.B "Ti Nuage" .
David doit les contacter.
.P
Pour chacun de ces forums, il devrait être possible d'employer les panneaux de l'
.I "Expolibre"
dont nous disposons.
Nourdine doit également reprendre les documents de l'association pour réaliser une plaquette fournissant les informations essentielles : les adresses pour accéder au site, découvrir l'association et les services, et les informations pour les réunions mensuelles.
.HU "Matériel"
À propos du FabLab, il semble à Nourdine qu'ils avaient parlé de possibilités de récupération de matériel. Dans le cadre de notre recherche pour réaliser notre implantation à Vieux-Marché, nous envisageons de les re-contacter pour savoir s'ils disposeraient de quelque chose d'intéressant. Et plus encore, de les mettre en relation avec la Hutte qui a probablement des besoins plus simples que les notres.
.HU "Site internet"
Le Wiki de l'association se remplit petit à petit grâce à Nourdine. Pour mémoire, ce Wiki est maintenant largement ouvert à tous.
Si vous avez besoin d'aide pour en profiter, rendez-vous à la prochaine réunion !
.P
Le site doit encore être amélioré, avec en particulier :
.BL
.LI
une refonte des boutons faisant office de menu en haut et du pied de page, afin de mieux mettre en évidence les liens utiles\ ;
.LI
l'amélioration de la page de services qui doit fournir pour chacun d'eux les informations du logiciel utilisé ainsi que le lien pour y accéder sur
.B "Ti Nuage" "\ ;"
.LI
la mention de notre candidature aux CHATONS.
.LE
.P
Ces évolutions sont l'occasion de rappeler que tout le monde peut utiliser les listes de diffusion de l'association pour mentionner un problème ou une erreur sur le site.
Ou, pour les plus explorateurs, de découvrir le système de suivi de tickets sur notre forge logicielle\*F
.FS
.URL "https://apps.ti-nuage.fr/gitea/ti-nuage/site/issues"
.FE
avec lequel nous traçons ce genre d'évolutions.
.HU "Atelier de Défense Numérique (exemple liégeois des Cafés Cryptés)"
Dans le cadre de nos activités mensuelles, Mathias mentionne, pour ceux qui s'intéressent à la défense numérique, les Cafés Cryptés de Liège\*F
.FS
.URL https://crypto.bawet.org
.FE
qui mettent à disposition un répertoire NextCloud\*F
.FS
.URL https://cloud.domainepublic.net/bawet/s/aq4GPibwDPrWA7G
.FE
contenant des documents relatifs à ce sujet.
Ces documents servent de support pour ces rendez-vous mensuels organisés en auto-gestion autour de ces thématiques (une fois par mois, quelques heures en fin de journée dans une cafétéria collective).
Tout un chacun peut consulter le contenu et en faire ce que bon lui semble (et la réutilisation est encouragée).
.B "Ti Nuage"
pourrait organiser un rendez-vous du même genre, à condition d'avoir des personnes motivées pour proposer ou tenir l'atelier.
.HU "Prochaine Assemblée Générale"
Soucieux de prendre soin du bureau très affaibli ces derniers mois, Nourdine attend avec impatience la prochaine Assemblé Générale.
Sa date est donc d'ores et déjà fixée au
.B "jeudi 6 octobre" ,
qui est également la date de notre rendez-vous mensuel pour le mois-là.
.P
Nous envisageons de voter au préalable la modification du chapitre concernant les électeurs afin de supprimer la nécessité d'une ancienneté de six mois et ainsi impliquer tous les adhérents, même les plus récents.
