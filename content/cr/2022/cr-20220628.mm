.ND 28/06/2022
.TL
Compte-rendu de Réunion
.br
Mardi 28 juin 2022 à Vieux-Marché
.AU "David, Mathias, Nourdine et Vincent L."
.AST "Résumé"
.AS
Une session sous le signe de l'appel à projets organisé par le département, dans le cadre de la démarche
.I "Construire le numérique en Côtes d’Armor" ,
mais aussi de l'évolution des services proposés.
Et toujours un point sur la candidature pour les CHATONS\*F
.FS
Le Collectif des Hébergeurs Alternatifs, Transparents, Neutres et Solidaires\ :
.URL https://www.chatons.org
.FE
qui se précise.
.P
Nous allons mieux définir le rendez-vous récurrent de l'association afin d'en faire un repère fixe chaque mois. Nous nous réunirons aussi de nouveau rapidement pour construire la réponse à apporter au département. Parce que la multiplication des regards est très positive pour ce dernier sujet, l'aide de tous est la bienvenue.
.AE
.MT 4
.hl
.PRESENT "David, Mathias, Nourdine, Vincent L."
.HU "Appel à projets pour le Numérique du CD22"
Le département a lancé un appel à projets dans le cadre de sa démarche
.I "Construire le numérique en Côtes d’Armor" :
.URL "https://cotesdarmor.fr/numerique"
\&.
Il s'agit de participer au financement d'un projet
.I "dans les territoires pour un Numérique pour tous, facilitant la vie quotidienne" .
Chaque projet peut être financé au maximum à 50% avec une enveloppe allant de 1000\[eu] à 5000\[eu].
La clôture de remise des dossiers est le 31 août.
Le dossier de candidature doit comporter :
.BL
.LI
une vidéo de deux minutes ;
.LI
un document de présentation du projet fournissant une justification, un budget, les coordonnées des responsables ;
.LI
un courrier mentionnant le soutien d'une commune ou d'une intercommunalité et justifiant le projet dans le projet du territoire.
.LE
.P
Nous pensons que l'association
.B "Ti Nuage"
est porteuse d'un tel projet, et l'aide du département accélèrerait la mise en place de nos propres serveurs.
Nous comptons donc répondre avec une description exacte de nos besoins matériels (achat de machines, de rack, de disques, etc.), et des frais attendus (installation de la fibre, qui devrait être disponible d'ici six mois environ à Vieux-Marché, peut-être une portion du loyer sur une certaine période).
Nous imaginons, pour la vidéo demandée, nous reposer sur les témoignages des adhérents qui le souhaitent, parler de l'hébergement des particuliers et des associations locales, et peut-être parler d'un rapprochement avec la Hutte.
.HU "Accès au compte bancaire"
Notre trésorier Vincent T. étant indisponible pour un temps indéfini, Nourdine a contacté la banque\*F
.FS
Au lendemain de la réunion.
.FE
afin de savoir comment gérer la situation et pouvoir accéder au compte bancaire.
Il s'avère que la situation n'est pas bloquée.
Nourdine doit maintenant recevoir une carte à puce comme celle dont dispose Vincent.
Et il lui faut récupérer le terminal de carte dont dispose ce dernier.
.HU "CHATONS"
Nourdine et David continuent le travail préparatoire afin d'annoncer notre candidature pour rejoindre le collectif.
Pour finaliser cette étape, Nourdine a produit un doument permettant de tracer les étapes devant encore être réalisées.
.P
En ce qui concerne les serveurs,
.I MailJet
doit toujours être remplacé afin d'adhérer à la charte des CHATONS.
La solution retenue est d'utiliser un VPN pour cela, et
.I Grifon
serait préféré à
.I FAImaison
(leurs services sont similaires, mais ce dernier tarde à répondre à nos demandes par courriel).
La mise en place est retardée par l'indisponibilité du trésorier, mais cela n'est pas immédiatement bloquant.
.HU "Rencontre des adhérents Ti Nuage"
Vincent L. rapporte que Vincent du bar de Vieux-Marché (que de Vincents dans ce compte-rendu) est partant pour accueillir les réunions et ateliers de
.B "Ti Nuage" .
.P
Nourdine suggère la mise en place d'une date récurrente une bonne fois pour toutes afin de créer un rendez-vous fixe et identifiable (ce qui ne remet pas en question la tenue d'événements ou de réunions exceptionnels à des dates complémentaires selon les besoins).
L'idée est approuvée ; charge à lui donc d'organiser un sondage afin de définir cette date pour les prochaines rencontres mensuelles de l'association et de ses adhérents.
.HU "Services de Ti Nuage"
\R'Hu +1'
.HU "Messagerie instantanée"
Les administrateurs travaillent depuis quelques semaines à remplacer
.I Mattermost ,
inutilement complexe et trop peu souple pour les besoins des adhérents.
Un document sur le Wiki détaille les expérimentations faites avec
.I Matrix
ou
.I XMPP
:
.URL https://wiki.ti-nuage.fr/Administration/EtudeMessagerieInstantanee
\&.
Le choix va se porter sur
.I XMPP ,
avec la mise à disposition dès maintenant de l'interface
.I Converse
sur le portail des adhérents.
(Mais l'intérêt d'une solution telle que XMPP réside aussi dans le choix des logiciels pour utiliser son compte : citons par exemple
.I Conversations
ou
.I Babbler.IM
sur
.I Android ).
.P
Les inscriptions à
.I Mattermost
sont dorénavant suspendues.
Son usage entre en période de transition durant laquelle les utilisateurs (principalement l'association
.B Liratouva )
sont accompagnés pour découvrir
.I XMPP .
.HU "Wiki"
Les administrateurs souhaitent que les adhérents aient la plus grande liberté possible sur le Wiki. Cela va dans le sens de l'éducation, l'entraide et l'autonomie portées par l'association.
En cas d'erreur, il est possible de se reposer sur l'historisation des pages.
.P
David propose quelques règles simples avec un contrôle minimal pour éviter l'éparpillement des pages et protéger les documents relatifs à l'administration ou à la gestion de l'association. Il doit maintenant les mettre en oeuvre.
.HU "Lutim"
.I Lutim
est suspendu depuis quelques semaines à cause des risques qu'il pose.
Il est décidé de le supprimer définitivement.
L'outil
.I "Firefox Send"
reste quant à lui en place.
Aucune précaution particulière n'est pour le moment actée dans son usage pour éviter les débordements.
Des contre-mesures et de fortes limitations seront étudiées à la première signalisation.
\R'Hu -1'
.HU "Forum des associations"
Il est envisagé de participer au Forum des associations de Vieux-Marché.
Les personnes présentes sont volontaires pour participer à la tenue du stand. Toutes les bonnes volontés pour passer un moment avec nous à cette occasion sont les bienvenues.
.P
La participation au forum de Lannion (le 3 septembre) avait également été envisagée.
David a envoyé un courriel à la mairie de Lannion pour savoir si une association localisée ailleurs pouvait être présente, mais de toutes manières la date limite d'inscription (le 26 juin) est dépassée.
