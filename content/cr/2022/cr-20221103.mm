.ND 03/11/2022
.TL
Compte-rendu de Réunion
.br
Jeudi 3 novembre 2022 à Vieux-Marché
.AU "David"
.AST "Résumé"
.AS
Vacances et impératifs familiaux obligent, voici le compte-rendu le plus court de l'année, pour relater une réunion réduite à deux personnes.
L'occasion de développer une ou deux idées propres à intéresser petites associations et visiteurs du site.
Idées qui ont besoin de volontaires pour être développées !
.AE
.MT 4
.hl
.PRESENT "David, Gilles"
.HU "Généralités"
L'audit réalisé par les CHATONS est maintenant bien avancé, et nous nous en tirons très bien.
David prend en compte les remarques concernant le site et les modalités de l'association, et Nourdine répond aux quelques questions techniques levées.
.P
Nourdine a envoyé un message pour connaître votre sentiment sur l'usage de Vaultwarden : nous nous demandons si une solution moins centralisée ne serait pas plus judicieuse, et moins risquée pour
.B "Ti Nuage" .
Par ailleurs, nous avons rencontré quelques petits problèmes avec Wallabag.
Le service est assez peu utilisé, mais si vous avez l'impression qu'il ne réagit pas comme il devrait, n'hésitez pas à vous manifester.
.HU "Un hébergement simplifié pour les associations"
Gilles propose de créer un modèle simple de site Web pour fournir aux associations une solution facile à prendre en main pour mettre en ligne leurs actualités, leurs informations.
Il faudrait qu'un petit nombre de personnes puisse tenir à jour le site, chacune pouvant éditer un aspect ou une section sans soucis de l'ensemble.
David suggère de se pencher du côté des petits CMS (
.B "htmly" ,
.B "Grav" ", ...)"
ou des logiciels permettant la construction de sites statiques (
.B "Pelican" ,
.B "metalsmith" ",...)."
La difficulté, avec ces derniers, est de trouver le moyen de regénérer simplement le site après chaque modification.
.HU "Un portail épuré pour tous"
Pour faire suite à l'idée de Gilles, David remet à l'ordre du jour l'idée de créer aussi un portail très simple pour les visiteurs, adhérents ou non, du site. Ce portail permettrait de faire une recherche sur un moteur de recherche plus discret que Google, et listerait les logiciels ou les services numériques les plus usuels\*F
.FS
Un exemple, chez un chaton :
.URL "https://portail.sans-nuage.fr/"
.FE
\&.
.HU "Matériel"
Gilles tient à la disposition de personnes intéressées des iPads dégradés.
David l'a invité à utiliser la liste de diffusion interne à l'association pour propager ce genre d'informations.
