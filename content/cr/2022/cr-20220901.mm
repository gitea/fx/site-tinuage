.ND 01/09/2022
.TL
Compte-rendu de Réunion
.br
Jeudi 1er septembre 2022 à Vieux-Marché
.AU "David"
.AST "Résumé"
.AS
Une réunion de rentrée en petit comité, avec beaucoup de discussions impromptues et un résumé des actions récentes. L'actualité, c'était surtout le forum aux associations de Plouaret, lequel est terminé au moment de la rédaction de ce compte-rendu ; ce court compte-rendu s'étoffe donc d'un petit retour sur l'événement.
.AE
.MT 4
.hl
.PRESENT "David, Mathias, Nourdine"
.HU "Subventions"
Le dossier pour l'appel à projets du département dans le cadre de sa démarche
.I "Construire le numérique en Côtes d’Armor"
a été envoyé en temps et en heure. Encore une fois, la description de cet appel et les raisons pour lesquelles nous y répondons se trouvent dans un précédent compte-rendu\*F
.FS
.URL "https://ti-nuage.fr/cr/2022/cr-20220628.pdf"
.FE
\&.
.P
Les dossiers retenus seront choisis en commission en octobre.
.P
Voilà une bonne chose de faite, mais ce dossier n'est qu'un début. En effet, le budget qu'il a fallu présenter pour répondre à cet appel à projets met en évidence que l'association est capable d'assumer son fonctionnement, mais que les investissements nécessaires pour l'implantation locale des serveurs nécssiterait de trouver d'autres subventions encore. Notre première démarche devrait naturellement se tourner vers l'agglomération. Nous recherchons donc la bonne personne à contacter chez LTC.
.HU "PeerTube"
Pour les besoins du dossier évoqué ci-dessus, il nous a fallu réaliser une courte vidéo (nous en parlions dans le compte-rendu précédent\*F
.FS
.URL "https://ti-nuage.fr/cr/2022/cr-20220804.pdf"
.FE
). Nous remercions chaleureusement tous les participants à cette vidéo, ainsi que Vincent pour sa réalisation. D'ailleurs, nous en sommes tellement contents que nous avons décidé de la diffuser.
.P
Cette vidéo sera peut-être suivie d'une poignée d'autres et nous n'avons pas l'intention dans l'immédiat de gérer une solution de diffusion de vidéos. Aussi Nourdine a-t-il passé en revue les instances PeerTube proposées par les CHATONS, et nous avons retenu
.B "TeDomum"
\*F
.FS
.URL "https://www.tedomum.net/"
.FE
pour son mode de fonctionnement et son sérieux. Plutôt que de faire un simple don, nous avons choisi une contribution mensuelle et opté en réunion pour un virement de 1€ par mois via Liberapay.
.HU "Événement à Plougrescant"
Grégoire, adhérent à l'association, a également répondu à l'appel à projets du département dans l'optique d'organiser une journée
.B "Faites du numérique"
à Plougrescant le 4 mars 2023. L'idée étant
.I "lors d'une journée de montrer les différentes facettes du numérique" .
Sont prévus mini-ateliers de MAO et de photographie, la présentation d'un robot Pepper, la présence du FabLab de Lannion... Nous avons également donné notre accord de principe pour être présents, et nous attendons d'en apprendre davantage.
.HU "De la technique..."
Aujourd'hui, les adhérents sont contactés par courriel avec une liste gérée par Sympa\*F
.FS
.URL "https://listes.ti-nuage.fr/sympa"
.FE
\&. Ils y sont abonnés et désabonnés à l'aide d'un script lors de la création ou de la suppression de leur compte. Nourdine a trouvé le moyen de créer un alias regroupant tous les utilisateurs du système. Avec ce dispositif, les adhérents auraient la possibilité de changer leur adresse de courriel. Mais quelques avantages de Sympa, comme la disponibilité de historique des courriels, seraient absents. Cette solution ne sera donc pas mise en place immédiatement.
.P
Nourdine a également apporté une mise à jour majeure sur le serveur qui supporte les principaux services. L'installation s'est correctement déroulée et aucun problème important n'a été rencontré.
.P
David recence les dernières améliorations apportées au site de l'association. Le menu et le pied de page sont plus clairs. La navigation au clavier pour les personnes mal-voyantes est un peu plus aisée avec une meilleure mise en évidence du
.I focus .
Notre candidature aux CHATONS est mise en avant. Comme déjà évoqué dans le dernier compte-rendu, il reste encore à remanier la page de services pour fournir des informations utiles sur les logiciels utilisés ou leur lien sur
.B "Ti Nuage" .
Il est aussi envisagé d'y insérer les événements présents sur le calendrier de l'association.
.P
Enfin, nous avions semble-t-il pour mission ce soir-là de résoudre un problème de Wifi à la Hutte. Malheureusement, le problème ne nous avait pas attendu, ou alors nous avons manqué de renseignements à son propos. L'occasion nous a toutefois permis de nous familiariser avec l'offre d'OVH et le routeur Zyxel employé.
.HU "Forum des associations"
Nous serons... Nous avons été présents au forum des association de Plouaret ce samedi 3 septembre. Nous avions envisagé d'être aussi présents à Lannion avec le FabLab, mais si ce dernier avait accueilli favorablement l'idée, il n'avait pas de stand cette année.
.P
Le forum de Plouaret se déroulait dans la cantine de l'école Jean Denis entre 9h et 12h. David, Mathias et Nourdine étaient présents toute la matinée avec quelques
.I "flyers"
ainsi que les panneaux de l'
.I "ExpoLibre"\*F
.FS
.URL "https://expolibre.org/"
.FE
\&.
La table présentait aussi la Hutte ainsi que l'association de jeux en devenir sur Vieux-Marché, ce qui n'a pas facilité la compréhension du public, mais a intéressé du monde.
.P
En somme, la matinée a été l'occasion de rencontrer quelques personnes intéressées, certaines semblant même assez motivées, et d'enregistrer une adhésion. Les
.I "flyers"
.B "Ti Nuage"
ont quasiment tous été distribués.
.P
L'accueil était parfait. Le forum s'accompagnait de démonstrations sportives qui ne débutaient qu'à 10 heures, mais duraient jusqu'au milieu de l'après-midi. Nous avons suggéré d'étendre les horaires du forum si les associations présentes étaient demandeuses.
.HU "Prochaine Assemblée Générale"
Comme annoncé déjà dans le dernier compte-rendu, la prochaine Assemblée Générale est fixée pour le
.B "jeudi 6 octobre" ,
qui est également la date de notre rendez-vous mensuel pour le mois-là.
.P
David enverra prochainement toutes les informations requises. Nous espérons vous y voir nombreux pour apprendre après une année d'existence ce qui vous convient, ce qui vous manque, et surtout pour recuillir vos remerciements et félicitations !
