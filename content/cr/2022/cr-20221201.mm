.ND 01/12/2022
.TL
Compte-rendu de Réunion
.br
Jeudi 1er décembre 2022 à Vieux-Marché
.AU "David, Mathias"
.AST "Résumé"
.AS
L'ordre du jour est assez copieux ce jeudi.
Mais l'essentiel de l'actualité, ce sont les résultats de l'Appel à projets numériques du département pour lequel
.B "Ti Nuage"
a été distingué.
Tout comme l'a été la proposition de Plougrescant d'organiser une manifestation sur les différents aspects du numérique dans notre quotidien.
Manifestation soutenue par Grégoire qui était parmi nous pour la première fois.
.AE
.MT 4
.hl
.PRESENT "Charlotte, David, Grégoire, Mathias, Nourdine, Vincent"
.HU "Compte-rendu de la cérémonie du CD22 du 25 novembre"
Le 25 novembre a eu lieu la remise des prix
.I "Appel du Numérique"
du CD22.
David et Nourdine étaient sur place et se sont vus remettre au nom de
.B "Ti Nuage"
un prix de 5000€.
Cette matinée a été l'occasion de rencontrer des acteurs institutionnels et d'autres de terrain.
Tous ces acteurs sont, du fait de leur distinction au terme de cet appel, membres du réseau
.I "Construire le numérique en Côtes d’Armor" ,
initié par le département.
Ils doivent figurer sur un annuaire spécifique et seront probablement sollicités pour des rencontres à l'avenir.
.P
L'association peut donc acheter du matériel et sera remboursée plus tard, à concurrence de 5000€. Cela pose la question de l'avance, et donc les rencontres avec d'autres acteurs doivent continuer.
.P
À cette occasion, l'association a bénéficié de plusieurs citations dans la presse.
Nous avons relevé les articles de Ouest France et du Trégor ; si vous en avez d'autres, n'hésitez pas à nous les envoyer.
Nous allons trouver le moyen de les mettre en valeur.
.HU "Journée du numérique à Plougrescant le 11 mars 2023"
Grégoire, adhérent à
.B "Ti Nuage"
et élu à Plougrescant, organise sur cette commune une journée du numérique ainsi que déjà relaté dans le compte-rendu du mois de septembre\*F
.FS
.URL "https://ti-nuage.fr/cr/2022/cr-20220901.pdf"
.FE
\&.
Il nous donne un peu plus de détails ce soir :
.BL
.LI
la date retenue est finalement le samedi 11 mars 2023, et cette première édition devrait se tenir sur une après-midi (de 13h30 à 18h30) ;
.LI
sont d'ores et déjà prévus des stands présentant du prototypage rapide, des démonstrations de réalité virtuelle (solution de VR de la BCA), des mini-ateliers autour de la MAO ou de la photo numérique ; parmi les invités se trouvent un thésard en sciences sociales qui présentera son travail autour d'un robot Pepper, ou le Fablab de Lannion (qui est partout).
.LE
.P
Proposition est faite à
.B "Ti Nuage"
de participer à l'événement pour parler de
.I "cloud souverain" ,
des dangers relatifs aux services numériques et de ses activités.
Il ne s'agit pas de préparer une intervention élaborée mais plutôt de tenir un stand et d'inviter les visiteurs à la discussion.
Plusieurs membres de l'association seront présents.
.P
Une réunion le 5 janvier prochain à Plougrescant avec les acteurs invités permettra de mieux définir cette journée..
.HU "Éducation au numérique"
Nourdine a rencontré des membres de l'équipe administrative du collège de Plouaret et il apparaît que ce dernier est en demande d'interventions autour de l'éducation au numérique auprès des élèves.
L'association répondra volontiers à cette demande, mais cela nécessite d'identifier les membres désireux de participer à de telles rencontres.
Les présents ce soir soulignent qu'ils auront besoin de connaître davantage la forme et le contenu souhaités par l'établissement, et qu'il faudra prendre le temps d'écrire un support de présentation correct et de s’entraîner au difficile exercice de l'intervention dans le milieu scolaire.
.P
Dans une forme similaire, l'inter-communalité nous demande de proposer quelque chose pour la
.I "Quinzaine de la parentalité" .
La question n'a pas beaucoup été débattue de soir, mais il faudra y revenir de manière plus pressante bientôt.
.HU "Retour sur la dernière Assemblée Générale"
Nourdine s'occupe activement de mettre l'association en conformité avec les administrations.
.BL
.LI
Les statuts et le procès-verbal de l'Assemblée Générale ont été signés ce soir ;
.LI
Le nouveau trésorier doit être inscrit à la banque.
.LE
.P
D'autres actions sont en cours suite à cette même Assemblée Générale :
.BL
.LI
Nourdine devrait contacter bientôt Roxane, qui a une activité de recherche de subventions pour des structures comme la notre ;
.LI
Julie, puis David se sont adressés à mesdames Mion et Furic de Lannion Trégor Communauté, mais nous n'avons pas encore obtenu de rendez-vous ;
.LI
Julie est prête à contacter le Pezh ou l'ADESS pour
.B "Ti Nuage" ,
afin de profiter de leur réseau et peut-être pour nous aider à rencontrer du monde afin de trouver un administrateur supplémentaire.
.LI
La question de modifier les statuts pour protéger l’association de toute prise de pouvoir intempestive est remise à plus tard, personne ne voyant pour le moment d'urgence à traiter ce point.
.LE
.HU "Une solution d'hébergement simplifié"
Gilles proposait le mois précédent\*F
.FS
.URL https://www.ti-nuage.fr/cr/2022/cr-20221103.pdf
.FE
de créer une solution simple pour l'hébergement de vitrines d'associations.
L'idée a séduit jusqu'au président.
Il s'agit maintenant d'identifier une solution, d'écrire une documentation et de réaliser des tests.
Le projet nécessite qu'une personne s'avance pour le mener au bout.
.HU "Le point sur les CHATONS"
La période d'audit s'est achevée le 15 novembre.
La période de vote pour l'admission des candidats a commencé le 1er décembre et court jusqu'au 20.
Au moment de la rédaction de ce compte-rendu nous n'avons que des retours positifs, aussi devrions-nous faire officiellement partie du collectif à la fin de l'année.
.HU "Questions techniques"
En marge de la réunion, Nourdine a continué à assister Charlotte, en particulier pour la prise en main d'un outil de forum, et pour la gestion des listes de courriel.
Nous vous rappelons que ces permanences mensuelles servent également à cela ; vous aider dans vos usages, répondre à vos questions ou essayer de résoudre vos problèmes.
.P
Nourdine explique aussi que le message de rappel de cotisation est aujourd'hui celui qui est configuré dans l'outil de gestion des adhérents (Dolibarr).
Il est assez impersonnel, et il est envoyé 10 jours avant la fin d'adhésion, le jour même, et encore 5 jours après.
Ce mécanisme ignore le fait que plusieurs comptes peuvent être liés à une seule structure, et en outre ces messages sont tous émis en copie vers les membres du bureau.
Il serait sympathique de proposer quelque chose d'un peu moins lourd et répétitif.
Il est décidé de réduire cet envoi automatique à la seule date d'expiration de l'adhésion, puis de traiter les relances de manière manuelle et plus personnalisée.
Il reste à voir comment cela va se mettre en place, et qui prend en charge le suivi de ces relances.
