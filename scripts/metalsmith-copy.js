const debug = require('debug')('metalsmith-copy')
const fs = require('fs')
const mode = require('stat-mode')
const path = require('path')

module.exports = plugin

function plugin (options) {
  options = Object.assign({ destination: '.' }, options)

  return function (files, metalsmith, done) {
    const dest = options.destination
    const items = options.files.length
    const promises = []

    const addTask = function (promise) {
      promises.push(promise)
      if (items === promises.length) {
        debug('Waiting for ' + items + 'copy operations.')
        Promise.all(promises)
          .then((data) => {
            done()
          })
          .catch((err) => {
            done(err)
          })
      }
    }

    options.files.forEach((filename) => {
      addTask(new Promise(function (resolve, reject) {
        fs.stat(filename, function (err, stats) {
          if (err) return reject(err)
          fs.readFile(filename, function (err, buffer) {
            if (err) return reject(err)
            const file = {
              contents: buffer,
              mode: mode(stats).toOctal()
            }
            files[path.join(dest, path.basename(filename))] = file
            resolve({})
          })
        })
      }))
    })
  }
}
