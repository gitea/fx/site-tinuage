const { resolve, join } = require('path')

const hostname = 'https://ti-nuage.fr'

const projectRoot = resolve(__dirname, '..')
const distribution = join(projectRoot, 'dist')

module.exports = {
  hostname,
  paths: {
    projectRoot,
    /* Nodes */
    leaflet: join(projectRoot, 'node_modules', 'leaflet', 'dist'),
    /* Metalsmith */
    metalsmithSource: 'content',
    metalsmithDestination: distribution,
    /* Server */
    serverRoot: distribution
  }
}
