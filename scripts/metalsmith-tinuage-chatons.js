// const { Buffer } = require('node:buffer')
const debug = require('debug')('metalsmith-tinuage-chatons')
const config = require('./config.js')
const fs = require('node:fs')
const path = require('path')

module.exports = plugin

/**
 * Un plugin pour Metalsmith qui traite des fichiers de propriétés
 * pour StatoolInfos.
 *
 * @return {Function}
 */
function plugin () {
  const Filler = {

    init: function (properties) {
      this.handlers = {
        'file.datetime': this.datetime,
        'file.generator': this.generator
      }
      this.properties = properties
    },

    complete: function () {
      Object.entries(this.properties).forEach(([key, value]) => {
        if (value.length === 0) {
          if (Object.keys(this.handlers).includes(key)) {
            this.properties[key] = this.handlers[key]()
          }
        }
      })
    },

    export: function () {
      return Object
        .entries(this.properties)
        .filter(([key, value]) => !key.startsWith('meta'))
        .map(([key, value]) => key + ' = ' + value)
        .reduce((acc, value) => acc + value + '\n', '')
    },

    datetime: function () {
      /* Statools n'attend pas de l'ISO8-601,
       * mais le strict format YYYY-MM-DDThh:mm:ss
       */
      const d = new Date()
      const pad = function (value) { return value.toString().padStart(2, '0') }
      const date = [d.getFullYear(), pad(d.getMonth() + 1), pad(d.getDate())]
      const time = [d.getHours(), d.getMinutes(), d.getSeconds()]

      return date.join('-') + 'T' + time.join(':')
    },

    generator: function () {
      return 'Metalsmith'
    }
  }

  function loadProperties (root, filenames) {
    const promises = filenames
      .filter(f => { return f.indexOf('.properties') !== -1 })
      .map(function (f) {
        debug('Loading ' + f)
        return fs.promises
          .readFile(path.join(root, f), 'utf8')
          .then(data => {
            const properties = Object.fromEntries(data
              .toString()
              .split(/(?:\r\n|\r|\n)/g)
              .filter(line => line.length > 1 && line[0] !== '#')
              .map(line => line.split(/\s*=\s*/, 2)))

            properties['meta.filename'] = f
            return properties
          })
      })

    return Promise.all(promises)
  }

  return function (files, metalsmith, done) {
    const directory = path.join('.well-known', 'chatonsinfos')
    const root = path.join(config.paths.projectRoot, config.paths.metalsmithSource, directory)

    debug('Looking for properties files in ' + root)
    fs.readdir(root, (err, filenames) => {
      if (err) {
        console.error('%s: %s', root, err)
      } else {
        loadProperties(root, filenames)
          .then(function (sets) {
            sets.forEach(properties => {
              const data = files[path.join(directory, properties['meta.filename'])]
              const filler = Object.create(Filler)

              filler.init(properties)
              filler.complete()

              data.contents = Buffer.from(filler.export())
            })
            done()
          })
          .catch(function (err) {
            console.log(err)
          })
      }
    })
  }
}
