/* This is the actual metalsmith configuration script. */
const assets = require('metalsmith-assets')
const browserify = require('metalsmith-browserify')
const cleanCSS = require('metalsmith-clean-css')
const chatons = require('./metalsmith-tinuage-chatons')
const config = require('./config.js')
const collections = require('@metalsmith/collections')
const copy = require('./metalsmith-copy')
const excerpts = require('@metalsmith/excerpts')
const groff = require('metalsmith-groff')
const layouts = require('@metalsmith/layouts')
const Metalsmith = require('metalsmith')
const markdown = require('@metalsmith/markdown')
const marked = require('marked')
const permalinks = require('@metalsmith/permalinks')
const rename = require('metalsmith-rename')
const services = require('./metalsmith-tinuage-services')
const sitemap = require('metalsmith-sitemap')
const slug = require('slug')
const statistics = require('./metalsmith-statistics-plugin')

const __PROD__ = process.env.NODE_ENV === 'production'

const markdownRenderer = new marked.Renderer()

const m = markdownRenderer.link

const { join } = require('path')

markdownRenderer.link = function (href, title, text) {
  let out = m.apply(this, [href, title, text])

  if (href.startsWith('http') && !href.match('^https?://(www.)?ti-nuage\\.fr/.*')) {
    out = out.slice(0, 3) + ' class="external" ' + out.slice(3)
  }

  return out
}

module.exports = new Metalsmith(config.paths.projectRoot)
  .clean(__PROD__)
  .metadata({
    date_format: function (date, withHours) {
      const options = { dateStyle: 'long' }
      if (withHours) {
        options.push('timeStyle', 'short')
      }
      return Intl.DateTimeFormat('fr', options).format(date)
    },
    production: __PROD__,
    remove_diatrics: function (e) { return slug(e, { mode: 'rfc3986' }) },
    site: {
      title: 'Ti Nuage'
    }
  })
  .source(config.paths.metalsmithSource)
  .destination(config.paths.metalsmithDestination)
  .use(cleanCSS({
    files: 'content/css/*.css',
    cleanCSS: {
      rebase: true
    }
  }))
  .use(browserify({
    browserifyOptions: {
      standalone: 'Calendar'
    },
    entries: [
      'js/calendar.js'
    ]
  }))
  .use(assets({
    source: './assets/' + (process.env.NODE_ENV || 'dev'),
    destination: './'
  }))
  .use(copy({
    files: [join(config.paths.leaflet, 'leaflet.css')],
    destination: './css'
  }))
  .use(copy({
    files: [
      join(config.paths.leaflet, 'images', 'marker-icon.png'),
      join(config.paths.leaflet, 'images', 'marker-icon-2x.png'),
      join(config.paths.leaflet, 'images', 'marker-shadow.png')
    ],
    destination: './css/images'
  }))
  .use(copy({
    files: [
      join(config.paths.leaflet, 'leaflet.js'),
      join(config.paths.leaflet, 'leaflet.js.map')
    ],
    destination: './js'
  }))
  .use(groff({
    macroPackages: ['mm', 'tinuage'],
    macroPaths: [join(config.paths.projectRoot, 'scripts/tmac')],
    preprocessors: ['tbl']
  }))
  .use(markdown({
    gfm: false,
    headerIds: false,
    renderer: markdownRenderer,
    smartypants: true,
    xhtml: true
  }))
  .use(chatons())
  .use(services())
  .use(excerpts())
  .use(permalinks({
    pattern: 'blog/:date/:title',
    date: 'YYYY/MM'
  }))
  .use(collections({
    archives: {
      pattern: 'blog/*/**/*/*.html',
      reverse: true,
      filterBy: (article) => {
        return new Date(article.date).getFullYear() !== new Date().getFullYear()
      },
      sortBy: 'date'
    },
    year: {
      pattern: 'blog/*/**/*/*.html',
      reverse: true,
      filterBy: (article) => {
        return new Date(article.date).getFullYear() === new Date().getFullYear()
      },
      sortBy: 'date'
    },
    news: {
      limit: 3,
      pattern: 'blog/*/**/*.html',
      reverse: true,
      sortBy: 'date'
    }
  }))
  .use(layouts({
    default: 'article.njk',
    pattern: '**/*.html',
    engineOptions: {
      filters: {
      },
      globals: {
        production: __PROD__
      }
    }
  }))
  .use(rename([
    [/\.njk$/, '.html']
  ]))
  .use(sitemap({
    changefreq: 'monthly',
    hostname: config.hostname
  }))
  .use(statistics())
