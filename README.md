# Code source du site internet de Ti Nuage

L'association Ti Nuage offre une palette des services numériques pour ses adhérents et les internautes, et promeut les logiciels libres et un usage éclairé et raisonné d'Internet.
Le site de l'association doit décrire ses missions et fournir un aperçu des services qu'elle propose, ainsi que de leurs conditions d'usage.

## Génération du site

Le site est créé à l'aide du logiciel libre [MetalSmith](http://www.metalsmith.io). Il s'agit d'un outil de transformation de fichiers qui permet en particulier la génération de sites statiques. L'intérêt de tels sites est leur facilité de déploiement, le peu d'usage qu'ils font en terme de ressources et leur robustesse. L'inconvénient est la nécessité de connaître quelques rudiments sur l'usage d'une ligne de commande pour régénérer et redéployer le site à chaque modification.

### Préparation des sources

**Metalsmith** requiert la disponibilité de **NodeJS** et **npm**. L'installation de ces logiciels dépend du système d'exploitation utilisé.

Les sources peuvent être téléchargées depuis le dépôt **Gitea** ou bien copiées localement à l'aide de **git** avec la commande suivante.

    git clone https://apps.ti-nuage.fr/gitea/ti-nuage/site.git

Le projet s'appuie sur un certain nombre de dépendances, utiles au développement ou à la génération du site lui-même. Pour installer ces dépendances :

    npm install

## Commandes

Le script `package.json` comprend une série de commandes utiles au développement et à la génération du site. Les plus utiles sont :

- pour générer le site : `npm run build`,
- pour le servir localement et le tester sur son navigateur : `npm run server`.

La liste des commandes existantes est disponible avec `npm run`. Il est possible d'obtenir davantage d'informations en cas de problème avec la variable d'environnement `DEBUG`. Par exemple :

    DEBUG=metalsmith* npm run build

## Structure

Le site de l'association est essentiellement composé de pages statiques. Les billets d'actualité et les comptes-rendus de réunion sont eux-mêmes générés ou assemblés avec le reste des pages du site.

Cependant, afin d'être réactif, l'affichage des événements du calendrier partagé de l'association est effectué depuis le navigateur. Si le navigateur dispose d'un support Javascript actif, alors :

- les événements à venir sont mixés avec les billets récents du blog dans la rubrique Actualité de la page d'accueil ;
- le menu propose une entrée « Calendrier » à la suite de « Actualité ». Cette nouvelle page présente plus largement les événements récents ou à venir.
